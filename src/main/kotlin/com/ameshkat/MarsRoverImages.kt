package com.ameshkat

import com.fasterxml.jackson.databind.ObjectMapper
import org.eclipse.microprofile.metrics.MetricUnits
import org.eclipse.microprofile.metrics.annotation.Counted
import org.eclipse.microprofile.metrics.annotation.Timed
import org.eclipse.microprofile.rest.client.inject.RestClient
import java.util.concurrent.CompletionStage
import javax.inject.Inject
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.QueryParam
import javax.ws.rs.core.MediaType

@Path("/marsUrls")
class MarsRoverImages(val objectMapper: ObjectMapper) {

    @Inject
    @field: RestClient
    internal lateinit var marsService: MarsService


    @GET
    @Counted(name = "performedChecks", description = "How many primality checks have been performed.")
    @Timed(name = "checksTimer", description = "A measure of how long it takes to perform the primality test.", unit = MetricUnits.MILLISECONDS)
    @Produces(MediaType.APPLICATION_JSON)
    fun getMars(@QueryParam("earth_date") earth_date: String): CompletionStage<List<MarsImageResponse>>? {

        return marsService.getMarsImageUrls(date = earth_date).thenApply {
            it.photos
        }.thenApply {
            it.map { photo ->
                MarsImageResponse(photo.img_src, photo.camera.name, roverStatus = photo.rover.status)
            }
        }

    }


}



