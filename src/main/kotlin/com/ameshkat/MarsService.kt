package com.ameshkat

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient
import java.util.concurrent.CompletionStage
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.QueryParam

@Path("/mars-photos/api/v1/rovers/curiosity/photos")
@RegisterRestClient
interface MarsService {


    @GET
    @Produces("application/json")
    open fun getMarsImageUrls(@QueryParam("earth_date") date: String = "2019-6-5",
                              @QueryParam("api_key") apiKey: String = "lVcUF5R3xcZAvnuTxG0SbeFFaRADWSLYtPCF9F5v"): CompletionStage<MarsRoverManifest>
}


