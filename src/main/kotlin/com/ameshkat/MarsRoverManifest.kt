package com.ameshkat

import io.quarkus.runtime.annotations.RegisterForReflection

@RegisterForReflection
data class MarsRoverManifest(var photos: MutableList<Photo> = mutableListOf<Photo>())

@RegisterForReflection
data class Camera(
        var full_name: String = "",
        var id: Int = 0,
        var name: String = "",
        var rover_id: Int = 0
)

@RegisterForReflection
data class Photo(
        var camera: Camera = Camera(),
        var earth_date: String = "",
        var id: Int = 0,
        var img_src: String = "",
        var rover: Rover = Rover(),
        var sol: Int = 0
)


@RegisterForReflection
data class CameraX(
        var full_name: String = "",
        var name: String = ""
)

@RegisterForReflection
data class Rover(
        var cameras: MutableList<CameraX> = mutableListOf(CameraX()),
        var id: Int = 0,
        var landing_date: String = "",
        var launch_date: String = "",
        var max_date: String = "",
        var max_sol: Int = 0,
        var name: String = "",
        var status: String = "",
        var total_photos: Int = 0
)

class MarsImageResponse(var img_src: String = "",
                        var cameraName: String = "",
                        var roverStatus: String = "")

