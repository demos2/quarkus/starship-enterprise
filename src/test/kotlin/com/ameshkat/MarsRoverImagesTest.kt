package com.ameshkat

import io.quarkus.test.junit.QuarkusTest
import io.restassured.RestAssured.given
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import java.time.LocalDate
import java.time.format.DateTimeFormatter

@QuarkusTest
open class MarsRoverImagesTest {

    @Test
    @Disabled
    fun testHelloEndpoint() {
        var formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
        val now = LocalDate.now().toString()
        var date = LocalDate.parse(
                now,
                formatter
        )

        given().queryParam("earth_date", date)
                .`when`().get("/marsUrls")
                .then()
                .statusCode(200)

    }

}
